This is a small sandbox for MVVM. Hopefully it will let you play with it a bit and understand the principle in a basic example.
These are the files you are concerned with. Look for some comments that start with MacDan in xaml.
GroupItemsPage.xaml (view)
GroupItemsPage.xaml.cs (view)
GroupItemsPageViewModel.cs (View Model)

I modified the default template to include a button/textblock in the upper left field which uses some MVVM principles.
Button has a text that comes from the view model.
Button also executes a command on the view model.
TextBlock has text that lives on the view model.

Flow:
1) View Model is created, and populates its fields.
2) View is created, and is initialized. It is bound the view models.
3) ... it updates its bindings with the view model. In this case, the button text and the Time.
4) User presses the button. It executes a command on the view model.
5) The view model is responsible to update the time, and notify that the update has changed.
6) Time should be updated on the UI.
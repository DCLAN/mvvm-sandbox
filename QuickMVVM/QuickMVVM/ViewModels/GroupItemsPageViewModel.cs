﻿using QuickMVVM.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace QuickMVVM.ViewModels
{
    /// <summary>
    /// MacDan: This is a basic view model.
    /// There is nothing very complex about it and will allow you to sand box it.
    /// </summary>
    public class GroupItemsPageViewModel : INotifyPropertyChanged
    {
        private String _formattedString;

        // This can contain other view models as well!
        public GroupItemsPageViewModel(string foo, string bar, DateTime time) 
        {
            Foo = foo;
            Bar = bar;
            Time = time;
            FormattedTime = formatDateTime(Time);

            // This is a command that currently gets run when a button is pressed
            // But in theory it could be any other operation coming from the UI
            UpdateTimeCommand = new RelayCommand(() =>
            {
                Time = DateTime.Now;
                FormattedTime = formatDateTime(Time);
            });

            Map = new ObservableDictionary();
        }

        /// <summary>
        /// This property will NOT refresh if you update it.
        /// </summary>
        public String Foo
        {
            get;
            set;
        }

        /// <summary>
        /// This property will NOT refresh if you update it.
        /// </summary>
        public String Bar
        {
            get;
            set;
        }

        public DateTime Time {
            get; 
            private set;
        }

        /// <summary>
        /// This property WILL refresh if you update it.
        /// </summary>
        public String FormattedTime
        {
            get
            {
                return _formattedString;
            }
            private set
            {
                _formattedString = value;

                // Implement INotifyPropertyChanged and that allows the values to be updated on  the UI.
                // The other properties won't get updated on the UI.
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("FormattedTime"));
                }
            }
        }

        private static String formatDateTime(DateTime time)
        {
            return string.Format("{0:hh:mm:ss}", time);
        }

        public ICommand UpdateTimeCommand
        {
            get;
            private set;
        }

        public ObservableDictionary Map
        {
            get;
            private set;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
